//Controllers contain the function and business logic of our express js app
//meaning all the operations can do will be placed in this file
const Task = require('../models/task');

//Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

//Controller Function for creating a task
module.exports.createTask = (requestBody) => {
	//Create a task object based on the Mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name 
	})

	//Save
	//The "then" method will accept the 2 parameters
		//The first parameter will store the result returned by the mongoose "save" method
		//the second parameter will store the "error" object
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			//If an error is encountered, the "return" statement will prevent any other line or code below and within the same code block from executing.

			//Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			//The else statement will no longer be evaluated
			return false;
		}else{
			//If save is successful return the new task object
			return task;
		}
	}) 
}

//Deleteing a task
/*
	Business Task
	1. Look for the task with the corresponding id provided in the URL/route
	2. Delete the task
*/

module.exports.deleteTask = (taskId) => {
	//The "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB it looks for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error){
			console.log(error);
			return false;
		}else{
			return removedTask;
		}
	})
}

//UPDATING A TASK
/*
	BUSINESS LOGIC
	1. Get the task with the ID
	2. Replace the task's name returned from the database with the "name" property
	3. Save the task
*/
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		//If an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}
			//Results of the "findById" method will be stored in the "result" parameter
			//It's "name" property will be reassigned the value of the "name received from the request"
			result.name = newContent.name;

			return result.save().then((updatedTask, error) => {
				if(error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				}
			})		
	})
}

/*
Activity
2.Create a controller function for retrieving a specific task.

	Business Logic
	1. Get the specific task with Id  
	2. Return the result
*/
module.exports.getSpecificTasks = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	})
}

/*Updating A Task for Activity
	6. Create a controller function for changing the status of a task to "complete".
	Business Logic
	1. Get the task with Id
	2. Change/Replace the task's status returned from the database with the status "complete"
	3. Save the task
*/
module.exports.updateTask = (taskId, newStatus) => {
	return Task.findById(taskId).then((result, error) => {
		
		if(error){
			console.log(error);
			return false;
		}
			
			result.status = newStatus.status;

			return result.save().then((updatedTask, error) => {
				if(error){
					console.log(error);
					return false
				}else{
					return updatedTask;
				}
			})		
	})
}
