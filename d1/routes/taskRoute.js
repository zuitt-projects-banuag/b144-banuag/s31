const express = require('express');
const taskController = require('../controllers/taskController')
//Create a Router instance that functions as a middleware and routing system
//Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

//Route to get all the tasks
//http://localhost:3001/tasks/

router.get("/", (req, res) => {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
}) 

//Create a route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})

//route for deleting a task
//http://localhost:3001/tasks/:id
//the colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL
//The word that comes after the colon symbol will be the name of the URL parameter
//":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
router.delete("/:id", (req, res) => {
	//URL parameter values are accepted via the request object's "params" property
	//The property name of this object will match the given URL parameter name
	//in this case "id" is the name of the parameter
	taskController.deleteTask(req.params.id).then(result => res.send(result));
})

//UPDATE A TASK
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})

/*
Activity
1. Create a route for getting a specific task.
*/
router.get("/:id", (req, res) => {
	taskController.getSpecificTasks(req.params.id, req.body).then(result => res.send(result));
}) 

/*
5. Create a route for changing the status of a task to "complete".
*/
router.put("/:id/complete", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})

module.exports = router;
